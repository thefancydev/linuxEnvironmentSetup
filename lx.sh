#!/bin/bash
version=$( cat $HOME/dev/linuxEnvironmentSetup/version )
POSITIONAL=()
while [[ $# -gt 0 ]]
do
	key="$1"

	case $key in
		-h|--help)
			echo "-h, --help: Displays this help message"
			echo "-u, --update: Checks for an update and installs it"
			echo "-l, --list: Lists all utilities"
			shift # past argument
		;;
		-u|--update)
			echo "Current version: $version"
		*)	# unknown option
			POSITIONAL+=("$1") # save it in an array for later
			shift # past argument
		;;
	esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

echo FILE EXTENSION  = "${EXTENSION}"
echo SEARCH PATH     = "${SEARCHPATH}"
echo LIBRARY PATH    = "${LIBPATH}"
echo DEFAULT         = "${DEFAULT}"
