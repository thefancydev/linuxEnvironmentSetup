#! /bin/bash

utildir=$HOME/linext
tools=$( ls -A $utildir/*.sh )
aliases=""


for tool in $tools;
do
	filename=$( basename $tool )
	aliasname=$( awk '{gsub(/.sh/, "")}1' <<< $filename )
	aliases="$aliasname='$tool'\n$aliases"
done

printf $aliases
