#! /bin/bash

if (( $EUID != 0)); then
	echo "Please run script as root"
	exit
fi

domainblock=false


mv "/etc/hosts" "/etc/hosts.tmp"
mv "/etc/hosts.polar" "/etc/hosts"
mv "/etc/hosts.tmp" "/etc/hosts.polar"

if [ grep -q "facebook.com" "/etc/hosts" ]
then
  $domainblock = true
fi
#no point in else, if it isn't blocked, domainblock is still false

if [ $domainblock == true ]
then
	echo "Swapped hosts files. currently running custom hosts with dns blocking."
else
	echo "Swapped hosts files. currently running default hosts file."
fi
