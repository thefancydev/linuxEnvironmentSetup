#! /bin/bash

#update vpn configs and login

if (( $EUID != 0)); then
	echo "Please run script as root"
	exit
fi

cd ./vpnconfig

./vpnbook config
./vpnbook auth

ls vpnbook-* > ./configlist

SELECTED=$(shuf -n 1 ./configlist)

openvpn --config $SELECTED --auth-user-pass ./vpnbook.auth
