#! /bin/bash


if [ $EUID != 0 ];
then
	echo "Please run this script as root"
	exit 0
fi

projectdir="$HOME/dev/linuxEnvironmentSetup"
setupdir="$HOME/linext"

#SETTINGS
echo "enabling system settings"
echo ""
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true #enabling touchpad on gnome DE
#END SETTINGS

#REPOS
echo "adding aptitude repositories"
echo ""
add-apt-repository ppa:wireshark-dev/stable
add-apt-repository ppa:webupd8team/sublime-text-3
#END REPOS

#PACKAGES
echo "Installing apt packages..."
echo ""
apt install sublime-text-installer golang nmap alien python python-pip ruby ruby-dev git git-flow curl
#END PACKAGES

#PACKAGE SETUP COMMANDS
echo "Setting up packages..."
echo ""
pip install flask
#END PACKAGE SETUP COMMANDS

#FOLDER STRUCTURE SETUP
cd ~
echo "Setting up directories ($setupdir)"
echo ""
mkdir $setupdir
mkdir $setupdir/vpnconfig
#END FOLDER STRUCTURE SETUP

#TOOL SETUP

#set up vpn
cd $HOME/dev
echo "Setting up VPN access..."
echo ""
git clone https://github.com/tobidope/vpnbook-utils.git
mv $HOME/dev/vpnbook-utils/vpnbook $setupdir/vpnconfig/

#set up default extension commands (hostblocker, backup, vpn, etc.)
tools=$( ls -a $projectdir/lx*.sh ) #all the scripts we want start with lx
echo "setting up default extension commands..."
echo ""
for tool in $tools;
do
		cp $tool $setupdir/
		name=$( basename $tool )
		echo "setting up $setupdir/$name"
done
